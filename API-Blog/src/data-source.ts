import { DataSource } from "typeorm"
import "dotenv/config"
import {User} from "./entities/users.entity"
import { Thoughts } from "./entities/thoughts.entity";
import { initialMigration1676046106572 } from "./migrations/1676046106572-initialMigration";
import { createTables1676046116549 } from "./migrations/1676046116549-createTables";



export const AppDataSource = new DataSource({
    type: 'postgres',
    host: process.env.DB_HOST,
    port: 5432,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB,
    synchronize: false,
    logging: true,
    entities: [User, Thoughts],
    migrations: [initialMigration1676046106572, createTables1676046116549]
});
AppDataSource.initialize().then(() => {
    console.log('Database connected')
}).catch((error) => {
    console.log(error)
});

