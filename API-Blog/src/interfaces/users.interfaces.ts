export interface IUserRequest {
    name: string
    email: string
    password: string
    thoughts: string
    isActive: true
};

export interface IUserResponse extends IUserRequest {
    id:  string
};

export interface IUserUpdateRequest {
    name?: string
    email?: string
    password?: string
    thoughts?: string
};