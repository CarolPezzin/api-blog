import { Request, Response, NextFunction } from 'express'

const ensureIsAdmMiddleware = async(req: Request, res: Response, next: NextFunction) => {
    
    const userId = req.user.id;
    const idParams = req.params.id;
      if (userId !== idParams) {
        return res.status(401).json({
          message: "User don't have access",
        });
      }
      next();
};
    

export default ensureIsAdmMiddleware