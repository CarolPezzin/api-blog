import { Request, Response, NextFunction } from 'express'
import { AppDataSource } from '../data-source';
import { Thoughts } from '../entities/thoughts.entity';

const ensureThoughtsUserMiddleware = async(req: Request, res: Response, next: NextFunction) => {
  const thoughtsRepository = AppDataSource.getRepository(Thoughts);
  const userId = req.user.id
  const idParams = req.params.id;
  console.log("Id do URL",idParams)
    const thoughts = await thoughtsRepository.findOne({
      where: {
        id: idParams,
      },
      relations: {
        user: true,
      },  
    });
    console.log("thoughts", thoughts?.user.id)
    const userThought = thoughts?.user.id
    console.log("user", userId)
    if (!thoughts) {
      return res.status(401).json({
          message: "User don't have access this thought!",
      })
    };
    if(userThought !== userId){
      return res.status(401).json({
        message: "User don't have access this thought!",
    })
    }
  next();
};
    

export default ensureThoughtsUserMiddleware