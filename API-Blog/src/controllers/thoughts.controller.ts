import { Request, Response } from "express";
import { Thoughts } from "../entities/thoughts.entity";
import { IThoughtsRequest } from "../interfaces/thoughts.interfaces";
import { thoughtsDeleteService, createThoughtsService, updatedThoughtsService, listUserThoughtsService, listThoughtsService } from "../services/thoughts.services";

const createThoughtsController = async(req: Request, res: Response) => {
    try {
        const data: IThoughtsRequest = req.body 
        const id = req.user.id
        const createdContact = await createThoughtsService(data, id)
        return res.json(createdContact)       
    } catch (error) {
        if(error instanceof Error){
            return res.status(400).json({
                message: error.message
            })
        }
    }
};

const listUserThoughtsConstroller = async( req: Request, res: Response) => {
    const usersThoughts = await listThoughtsService()
    return res.json(usersThoughts)
}

const listThoughtsController = async(req: Request, res: Response) => {
    const id = req.user.id
    const contacts = await listUserThoughtsService(id)
    return res.json(contacts)
};

const updateThoughtsController = async (req: Request, res: Response) => {
    try {
        const contact = req.body
        const id: string = req.params.id
        const updatedContact = await updatedThoughtsService(contact, id)
        if(updatedContact instanceof Thoughts){
            return res.json(updatedContact)
        }
        return res.status(updatedContact[1] as number).json({
            message: updatedContact[0]
        })
    } catch (error) {
        if(error instanceof Error){
            return res.status(400).json({
                message: error.message
            })
        }
    }
};

const thoughtsDeleteController = async (req: Request, res: Response) => {

    const {id} = req.params
    
        const deletedId =  await thoughtsDeleteService(id)
        
        return res.status(204).json({deletedId, message: "User deleted with sucess!"}) 
 
}

export {createThoughtsController, listThoughtsController, listUserThoughtsConstroller, updateThoughtsController, thoughtsDeleteController}