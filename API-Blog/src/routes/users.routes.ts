import { Router } from "express";
import { createThoughtsController, listThoughtsController, listUserThoughtsConstroller, thoughtsDeleteController, updateThoughtsController } from "../controllers/thoughts.controller";
import { listUserConstroller, updateUserController, userDeleteController } from "../controllers/users.controllers";
import { createUserController } from "../controllers/users.controllers";
import ensureIsAdmMiddleware from "../middlewares/ensureAdm.middlewares";

import ensureAuthMiddleware from "../middlewares/ensureAuth.middlewares";
import ensureThoughtsUserMiddleware from "../middlewares/ensureThoughtsUser.middlewares";

const userRoutes = Router();

userRoutes.post('', createUserController);
userRoutes.get('', ensureAuthMiddleware, listUserConstroller);
userRoutes.patch('/:id', ensureAuthMiddleware, ensureIsAdmMiddleware, updateUserController);
userRoutes.delete('/:id', ensureAuthMiddleware, ensureIsAdmMiddleware, userDeleteController);

userRoutes.post('/thoughts/:id', ensureAuthMiddleware, ensureIsAdmMiddleware, createThoughtsController);
userRoutes.get('/thoughts/:id', ensureAuthMiddleware, listThoughtsController);
userRoutes.get('/thoughts', ensureAuthMiddleware, listUserThoughtsConstroller);
userRoutes.patch('/thoughts/:id', ensureAuthMiddleware, ensureThoughtsUserMiddleware, updateThoughtsController);
userRoutes.delete('/thoughts/:id', ensureAuthMiddleware, ensureThoughtsUserMiddleware, thoughtsDeleteController);



export default userRoutes