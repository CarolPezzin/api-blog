import { Entity, Column, CreateDateColumn, PrimaryGeneratedColumn, ManyToOne, UpdateDateColumn } from "typeorm";
import { User } from "./users.entity";
import { v4 as uuid } from "uuid";

@Entity('thoughts')
class Thoughts {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({ length: 4000 })
    thoughts: string

    @CreateDateColumn()
    createdAt: Date

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => User)
    user: User;
}

export { Thoughts }