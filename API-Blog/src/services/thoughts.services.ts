import { FindRelationsNotFoundError } from "typeorm";
import { AppDataSource } from "../data-source";
import { Thoughts } from "../entities/thoughts.entity";
import { User } from "../entities/users.entity";
import { IThoughtsRequest } from "../interfaces/thoughts.interfaces";

const createThoughtsService = async (
  { thoughts }: IThoughtsRequest,
  id: string
) => {
  const userRepository = AppDataSource.getRepository(User);
  const contactRepository = AppDataSource.getRepository(Thoughts);

  const user = await userRepository.findOneBy({
    id,
  });

  const contact = contactRepository.create({
    thoughts,
    user: user!,
  });
  await contactRepository.save(contact);

  return contact;
};

const listUserThoughtsService = async (id: string): Promise<Thoughts[]> => {
  const userRepository = AppDataSource.getRepository(User);

  const user = await userRepository.findOne({
    where: {
      id,
    },
    relations: {
      thoughts: true,
    },
  });

  return user?.thoughts!;
};

export const listThoughtsService = async(): Promise<Thoughts[]> => {
  const thoughtsRepository = AppDataSource.getRepository(Thoughts)
  const thought = await thoughtsRepository.find()
  return thought
};

const updatedThoughtsService = async (
  { thoughts }: IThoughtsRequest,
  id: string
): Promise<Thoughts | Array<string | number>> => {
  const thoughtsRepository = AppDataSource.getRepository(Thoughts);
   
  const findThoughts = await thoughtsRepository.findOneBy({
    id,
  });

  if (!findThoughts) {
    return ["Thoughts not found", 404];
  }
  

  await thoughtsRepository.update(id, {
    thoughts: thoughts ? thoughts : findThoughts.thoughts,
  });

  const thought = await thoughtsRepository.findOneBy({
    id,
  });

  return thought!;
};

const thoughtsDeleteService = async (id: string) => {
  const thoughtsRepository = AppDataSource.getRepository(Thoughts);

  const thoughts = await thoughtsRepository.findOne({
    where: {
      id: id,
    },
  });

  if (!thoughts) {
    throw new Error("thoughts not found");
  }

  await thoughtsRepository.delete(thoughts!.id);

  return true;
};

export {
  createThoughtsService,
  listUserThoughtsService,
  updatedThoughtsService,
  thoughtsDeleteService,
};
