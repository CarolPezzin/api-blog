# Documentação da API-Blog

1.  Visão geral do projeto:
    . NodeJS
    . Express
    . TypeScript
    . PostgreSQL
    . TypeORM
    . Yup
    A URL base da aplicação: http://localhost:3000/users

2.  Diagrama ER
    Diagrama ER da API definindo bem as relações entre as tabelas do banco de dados.
    ![Diagrama utilizado na API](/diagramaAPI-Blog.png "Diagrama")

3.  Instalando dependências
    yarn
    cp .env.example .env (variáveis de ambiente)
    yarn typeorm migration:run -d src/data-source.ts (comando das migrations)

4.  EndPoints
    - Users
        . POST - /users
        . GET - /users
        . PATCH - /users/:user_id
        . DELETE - /users/:user_id
    - Thoughts
        . POST - /users/thoughts/:user_id
        . GET - /users/thoughts
        . GET - /users/thoughts/:thoughts_id
        . PATCH - /users/thoughts/:thoughts_id
        . DELETE - /users/thoughts/:thoughts_id

    4.1 Criação de usuários:
        Request:
            POST /users
            Host: http:localhost:3000
            Authorization: None

        Corpo da Requisição:

            {
                "name": "",
                "email": "",
                "password": ""
            }
        
        Resposta da Requisição:
        200 Created
            {
                "isActive": true,
                "name": "Miriam",
                "email": "miriam@email.com",
                "password": "$2b$10$eCjkD22fOZONGdChTFVmtuoPwq0gGSeozI1DmflSwqLjxZMzFsNHK",
                "id": "45916573-197a-4483-b1b3-b09b27acc14c",
                "createdAt": "2023-02-10T16:44:56.264Z",
                "updatedAt": "2023-02-10T16:44:56.264Z"
            }
        
    4.2 Listando Usuários
        /users
        Request:
            GET /users
            Host: http:localhost:3000
            Authorization: None
        Response:
        200 OK
            [
                {
                    "isActive": true,
                    "id": "1a4657b0-afe5-4e87-bc7c-151ccf169be4",
                    "name": "Crigor",
                    "email": "crigor@email.com",
                    "password": "$2b$10$vHzW.g9lSvRqshMfIFOXVeDTvuusHBzc7BAoIh9iHq8aPfTWZBc0O",
                    "createdAt": "2023-02-10T16:23:04.694Z",
                    "updatedAt": "2023-02-10T16:23:04.694Z"
                },
                {
                    "isActive": true,
                    "id": "fe060dbe-ea77-4473-9919-d158b9119143",
                    "name": "Tobias",
                    "email": "tobias@email.com",
                    "password": "$2b$10$W9l8kD9FEmHgLovZeKvmtuI7Lu/c310.XCV1a9yi73LJIECKsyZKS",
                    "createdAt": "2023-02-10T16:23:17.841Z",
                    "updatedAt": "2023-02-10T16:23:17.841Z"
                },
                {
                    "isActive": true,
                    "id": "45916573-197a-4483-b1b3-b09b27acc14c",
                    "name": "Miriam",
                    "email": "miriam@email.com",
                    "password": "$2b$10$eCjkD22fOZONGdChTFVmtuoPwq0gGSeozI1DmflSwqLjxZMzFsNHK",
                    "createdAt": "2023-02-10T16:44:56.264Z",
                    "updatedAt": "2023-02-10T16:44:56.264Z"
                }
            ]

    4.3 Atualização de usuarios por ID apenas o próprio usuário
        /users/:user_id
        PATCH /users/user_id
        Host: http:localhost:3000
        Authorization: Sim
        Request:
            {
                "name": "",
                "email": "",
                "password": ""
            }
        Response:
        200 OK
            {
                "isActive": true,
                "id": "1f348479-d674-41a2-ad82-bab5795ea36c",
                "name": "NomeAlterado",
                "email": "",
                "password": "Password Alterado",
                "createdAt": "2023-02-01T15:27:03.212Z",
                "updatedAt": "2023-02-07T14:26:59.242Z"
            }   
    4.4 Delete de usuarios por ID apenas o próprio usuário - usuário irá sofrer um soft delete permanecendo no bd não mais ativo.
        /users/:user_id
        DELETE /users/user_id
        Host: http:localhost:3000
        Authorization: Sim
        Request:
        Response:
        204 No Content
    
    4.5 Possíveis erros Users
        PATCH /users/user_id - 401 Unauthorized
            {
                "message": "User don't have access"
            }
        DELETE /users/user_id - 401 Unauthorized
            {
                "message": "User don't have access"
            }
    
    4.6 Criação de citações:
        Request:
            POST /users/thoughts/:user_id
            Host: http:localhost:3000
            Authorization: None

        Corpo da Requisição:
            {
                "thoughts": "Olá, eu sou a Miriam"
            }
        Response da Requisição:
        200 Created
            {
                "thoughts": "Olá, eu sou a Miriam",
                "user": {
                    "isActive": true,
                    "id": "45916573-197a-4483-b1b3-b09b27acc14c",
                    "name": "Miriam",
                    "email": "miriam@email.com",
                    "password": "$2b$10$eCjkD22fOZONGdChTFVmtuoPwq0gGSeozI1DmflSwqLjxZMzFsNHK",
                    "createdAt": "2023-02-10T16:44:56.264Z",
                    "updatedAt": "2023-02-10T16:44:56.264Z"
                },
                "id": "c9ca621e-6219-4e78-a8ed-6cccd9acb196",
                "createdAt": "2023-02-10T16:45:31.585Z",
                "updatedAt": "2023-02-10T16:45:31.585Z"
            }
    4.7 Listando Citações do usuário logado
        /users/thoughts/:user_id
        Request:
            GET /users/thoughts/user_id
            Host: http:localhost:3000
            Authorization: None
        Response:
        200 OK
            [
                {
                    "id": "c9ca621e-6219-4e78-a8ed-6cccd9acb196",
                    "thoughts": "Olá, eu sou a Miriam.",
                    "createdAt": "2023-02-10T16:45:31.585Z",
                    "updatedAt": "2023-02-10T16:45:58.841Z"
                }
            ]

    4.8 Listando Citações de Todos
        /users/thoughts
        Request:
            GET /users/thoughts
            Host: http:localhost:3000
            Authorization: None
        Response:
        200 OK
            [
                {
                    "id": "d938dbf8-4416-46ca-a29e-4598404b7827",
                    "thoughts": "Olá, eu sou o Tobias, e agora sim deve atualizar.",
                    "createdAt": "2023-02-10T16:23:57.365Z",
                    "updatedAt": "2023-02-10T16:42:02.243Z"
                },
                {
                    "id": "8b0c68de-d26b-4a0b-add0-bfbaaa8ff314",
                    "thoughts": "Olá, eu sou o Crigor.",
                    "createdAt": "2023-02-10T16:42:58.154Z",
                    "updatedAt": "2023-02-10T16:43:26.499Z"
                },
                {
                    "id": "c9ca621e-6219-4e78-a8ed-6cccd9acb196",
                    "thoughts": "Olá, eu sou a Miriam.",
                    "createdAt": "2023-02-10T16:45:31.585Z",
                    "updatedAt": "2023-02-10T16:45:58.841Z"
                }
            ]

    4.9 Atualização de citações por ID, apenas o próprio usuário.
        /users/thoughts/:thoughts_id
        PATCH /users/thoughts/thoughts_id
        Host: http:localhost:3000
        Authorization: Sim
        Request:
            {
                "thoughts": "Olá teste de atualização da Miriam"
            }
        Response:
        200 OK
            {
                "id": "c9ca621e-6219-4e78-a8ed-6cccd9acb196",
                "thoughts": "Olá teste de atualização da Miriam.",
                "createdAt": "2023-02-10T16:45:31.585Z",
                "updatedAt": "2023-02-10T16:45:58.841Z"
            }

    4.10 Delete de citações por ID, apenas o próprio usuário.
        /users/thoughts/:thoughts_id
        DELETE /users/thoughts/thoughts_id
        Host: http:localhost:3000
        Authorization: Sim
        Request:
        Response:
        204 No Content
    
    4.11 Possíveis erros Citações
        PATCH /users/thoughts/thoughts_id - 401 Unauthorized
            {
                "message": "User don't have access this thought!"
            }
        DELETE /users/thoughts/thoughts_id - 401 Unauthorized
            {
                "message": "User don't have access this thought!"
            }
